import { EventEmitter } from 'events'

const store = new EventEmitter()
export default store

let state = {
	uploadOpen: false
}

store.state = state

store.openUpload = () => {
	console.log('open it')
	state.uploadOpen = true
}

store.closeUpload = () => {
	console.log('close it')
	state.uploadOpen = false
}
