import Vue from 'vue'
import Router from 'vue-router'
import App from './App'
import Navbar from './components/Navbar'
import i18n from 'vue-i18n'
/* eslint-disable no-new */
/*
new Vue({
	el: 'body',
	components: { App }
})
*/
Vue.use(i18n)
Vue.use(Router)
Vue.use(require('vue-resource'))

Vue.ready = () => {
}

// i18n config
const locales = {
	en: {
		menu: {
			home: 'Home',
			login: 'Login',
			upload: 'Upload'
		},
		message: {
			greeting: 'Hi, how are you?'
		}
	},
	is: {
		menu: {
			home: 'Heim',
			login: 'Innskráning'
		},
		message: {
			greeting: 'Verið velkomin!'
		}
	}
}
Vue.config.lang = 'en'
Object.keys(locales).forEach(function (lang) {
	Vue.locale(lang, locales[lang])
})

// routing
let router = new Router()
router.map({
	'/foo': {
		component: Navbar
	}
})

router.start(App, 'app')
